<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Abubacker&middot;Siddik&middot;A</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="This is a personal website of Abubacker Siddik A, to show case his technical skills, interests, projects, community contributions and current activities etc. This will express his profile to outer world to get recognition and identity.">
    <meta name="keywords" content="abuabdul, abubacker siddik, bigdata, hadoop, java, j2ee, scalafx, scala">
    <meta name="author" content="Abubacker Siddik A">
    <meta name="copyright" content="&copy; abuabdul.com 2013-2014">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="msvalidate.01" content="DD6D0FA1C43C0EF96FF8026EB4049C15" />
    <meta charset="UTF-8">

<!-- Le styles -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"/>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/apps.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

   <!-- Font Awesome support for IE7 -->
    <!--[if IE 7]>
      <link rel="stylesheet" href="font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

   <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->
  </head>
  <body>
		<%@include file="WEB-INF/pages/header.jsp" %>
    <div class="container-fluid">
      <div class="row-fluid">
          <%@include file="WEB-INF/pages/sidemenu.jsp" %>
        <div class="span9">
           <%@include file="WEB-INF/pages/intro.jsp" %>
           <%@include file="WEB-INF/pages/ecoskills.jsp" %>
        </div><!--/span-->
      </div><!--/row-->

      <hr>
      
      <footer>
        <p>&copy; abuabdul.com 2013-2016</p>
      </footer>
    
    </div><!--/.fluid-container -->
      
     <%@include file="WEB-INF/pages/pagescript.jsp" %>
  </body>
</html>
