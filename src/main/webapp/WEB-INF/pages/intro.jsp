<%-- This page has the introduction about the author --%>
<div class="hero-unit hero-unit_pad_change shadow-box">
	<div class="row-fluid">
		<div class="span7">
			<p>
				Hi! I am an expert in Java/J2EE Technologies. <br /> I 
				design and code Java based web applications. <br /> I am also experienced 
				in Big Data, Hadoop MapReduce framework. I focus towards data technologies 
				that help solve big data problems. <br />
				<br /> I am passionate about learning new technologies, frameworks
				and open source applications. I love to read books. I am active in open 
				source communities.
			</p>
		</div>
		<div class="span4">
			<img src="images/me.JPG" alt="Abubacker Siddik A"
				class="img-polaroid" />
		</div>
	</div>
</div>