<%-- This page contains the java skills source --%>
<div class="row-fluid show-grid">
	<div class="span12">
		Core Java
		<div class="row-fluid show-grid">
			<div class="span4">Hibernate</div>
			<div class="span4">Struts</div>
			<div class="span4">DWR</div>
		</div>
		<div class="row-fluid show-grid">
			<div class="span12">Spring</div>
		</div>
		<div class="row-fluid show-grid">
			<div class="span12">
				J2EE API
				<div class="row-fluid show-grid">
					<div class="span6">Servlet</div>
					<div class="span6">JSP</div>
				</div>
			</div>
		</div>
		<div class="row-fluid show-grid">
			<div class="span12">
				J2EE Compliant Servers
				<div class="row-fluid show-grid">
					<div class="span4">Weblogic Server</div>
					<div class="span4">Tomcat Server</div>
					<div class="span4">Jetty Server</div>
				</div>
			</div>
		</div>
	</div>
</div>