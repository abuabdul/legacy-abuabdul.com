<%-- This page contains hadoop skills source--%>
<div class="row-fluid show-grid">
	<div class="span12">
		Hadoop
		<div class="row-fluid show-grid">
			<div class="span6">Hadoop MapReduce</div>
			<div class="span6">HDFS</div>
		</div>
		<div class="row-fluid show-grid">
			<div class="span4">Pig</div>
			<div class="span4">Hive</div>
			<div class="span4">HBase</div>
		</div>
	</div>
</div>