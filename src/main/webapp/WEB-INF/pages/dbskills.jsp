<%-- This page contains DB skills source--%>
<div class="row-fluid show-grid">
	<div class="span12">
		RDBMS
		<div class="row-fluid show-grid">
			<div class="span6">MySQL</div>
			<div class="span6">Oracle</div>
		</div>
	</div>
</div>

<div class="row-fluid show-grid">
	<div class="span12">
		NoSQL
		<div class="row-fluid show-grid">
			<div class="span12">MongoDB</div>
		</div>
	</div>
</div>
