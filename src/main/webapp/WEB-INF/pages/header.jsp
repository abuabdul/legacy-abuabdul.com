<%-- This page contains the banner and header of the site --%>

<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<button type="button" class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="brand" href="#">Abubacker Siddik</a>
			<div class="nav-collapse collapse">
				<p class="navbar-text pull-right margin-top">
					<!-- Last Updated on 09/08/2013 -->
					<span class="tooltip-place"> <a href="#"
						data-toggle="tooltip" data-placement="bottom"
						title="as of 01/05/2016" class="label"> <i
							class="icon-refresh"></i>
					</a>
					</span>

				</p>
				<ul class="nav">
					<li class="active"><a href=""> <span class="icon-stack">
								<i class="icon-check-empty icon-stack-base"></i> <i
								class="icon-home"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a
						href="mailto:abuabdul@abuabdul.com" data-toggle="tooltip"
						data-placement="bottom"
						title="Write to me @ abuabdul@abuabdul.com"> <span
							class="icon-stack"> <i
								class="icon-check-empty icon-stack-base"></i> <i
								class="icon-envelope"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a href="https://blogs.abuabdul.com"
						data-toggle="tooltip" data-placement="bottom" title="My Blog" target="_blank">
							<span class="icon-stack"> <i
								class="icon-check-empty icon-stack-base"></i> <!-- <i class="icon-file-text"></i> -->
								<i class="icon-edit"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a href="http://abubackersiddik.tumblr.com"
						data-toggle="tooltip" data-placement="bottom" title="Tumblr blog" target="_blank">
						 <span class="icon-stack"> 
							   <i class="icon-check-empty icon-stack-base"></i>
							   <i class="icon-tumblr"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a href="http://techie.abuabdul.com"
						data-toggle="tooltip" data-placement="bottom" title="Tech Blog" target="_blank">
						 <span class="icon-stack"> 
							   <i class="icon-check-empty icon-stack-base"></i>
							   <i class="icon-keyboard"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a
						href="http://www.linkedin.com/in/abubackersiddik"
						data-toggle="tooltip" data-placement="bottom" title="LinkedIn" target="_blank">
							<span class="icon-stack"> <i
								class="icon-check-empty icon-stack-base"></i> <i
								class="icon-linkedin"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a
						href="https://twitter.com/abubackersiddik" data-toggle="tooltip"
						data-placement="bottom" title="Twitter" target="_blank"> <span
							class="icon-stack"> <i
								class="icon-check-empty icon-stack-base"></i> <i
								class="icon-twitter"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a
						href="https://trello.com/abubackersiddik" data-toggle="tooltip"
						data-placement="bottom" title="Trello" target="_blank"> <span
							class="icon-stack"> <i
								class="icon-check-empty icon-stack-base"></i> <i
								class="icon-trello"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a
						href="http://stackoverflow.com/users/2112490/abubacker-siddik"
						data-toggle="tooltip" data-placement="bottom"
						title="StackOverFlow" target="_blank"> <span class="icon-stack"> <i
								class="icon-check-empty icon-stack-base"></i> <i
								class="icon-stackexchange"></i>
						</span>
					</a></li>
					<li class="tooltip-place"><a
						href="https://github.com/abuabdul" data-toggle="tooltip"
						data-placement="bottom" title="GitHub" target="_blank"> <span
							class="icon-stack"> <i
								class="icon-check-empty icon-stack-base"></i> <i
								class="icon-github"></i>
						</span>
					</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
</div>