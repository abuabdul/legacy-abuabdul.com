<%-- This is side menu page --%>
<div class="span3">
	<div class="well sidebar-nav shadow-box">
		<ul id="menunav" class="nav nav-list">
			<li class="nav-header">Java</li>
			<li><a href="#">Castor Exercise</a></li>
			<li><a href="https://blogs.abuabdul.com/2014/01/a-knowledge-indexing-tool-knodex/" target="_blank">Knodex</a></li>
			<li><a href="http://abuabdul.com/FourT" target="_blank">FourT</a></li>
			<li><a href="http://abuabdul.com/note-dovn" target="_blank">note-dovn</a></li>
			<li class="nav-header">Scala</li>
			<li><a href="#">ScalaFX Ensemble</a></li>
			<li><a href="#">ThoughtWriter</a></li>
			<li class="nav-header">Bootstrap</li>
			<li><a href="https://blogs.abuabdul.com/2014/01/feature-showcase-bootstrap/" target="_blank">Bootstrap Showcase</a></li>
		</ul>
	</div>
	<!--/.well -->
	<div class="well sidebar-nav shadow-box">
		<ul id="menunav" class="nav nav-list">
			<li class="nav-header">Community Activities</li>
			<li><a href="#">Talk on HOF in Scala meetup</a></li>
			<li class="text-right"><a href="#"><span
					class="label label-info">Read More..</span></a></li>
		</ul>
	</div>
	<div class="well sidebar-nav shadow-box">
		<ul id="menunav" class="nav nav-list">
			<li class="nav-header">Recent Activities</li>
			<li><a href="#">Preparing for SCJP 1.6..</a></li>
			<li><a href="#">Enrolled Algorithm class..</a></li>
			<li><a href="#">Reading Spring in Action 3.0..</a></li>
			<li><a href="#">Screening the candidates..</a></li>
			<li class="text-right"><a href="#"><span
					class="label label-info">Read More..</span></a></li>
		</ul>
	</div>
	<!--/.well -->
</div>
<!--/span-->
